const assert  = require("assert");
const fetch = require("node-fetch")
const Request = require("@dollarshaveclub/cloudworker").Request;

describe("GET /", function() {

    it("IP outside the allow-list", async function() {
        let res
        res = await fetch(process.env.DESTINATION, {
            headers: {
                "x-debug": true,
                "x-ip" : "193.104.162.7"
            }
            });
        assert.equal(res.headers.get("maintenance"), 1);
        
    });

    it("IP inside the allow-list", async function() {
        let res
        res = await fetch(process.env.DESTINATION, {
            headers: {
                "x-debug": true,
                "x-ip" : "8.8.8.8"
            }
            });
        assert.equal(res.headers.get("maintenance"), null);
    });
});